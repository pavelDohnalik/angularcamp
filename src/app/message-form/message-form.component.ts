import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {MessagesService} from "../messages-service.service";
import {MessageModel} from "../message/MessageModel";

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.css']
})
export class MessageFormComponent implements OnInit {

  errorMessage;

  public form = this.formBuilder.group({
    name: ['', [Validators.required]],
    color: ['#', [Validators.pattern("^#[0-9a-f]{3,6}$"), Validators.required]],
    text: ['', [Validators.required]]
  });

  constructor(private formBuilder: FormBuilder, private messageService:MessagesService) {
    this.form.valueChanges.subscribe( () => {
        this.errorMessage = "Invalid: ";
        if (!this.form.get('name').valid) {
          this.errorMessage = this.errorMessage + "Name "
        }
        if (!this.form.get('color').valid) {
          this.errorMessage = this.errorMessage + "Color "
        }
        if (!this.form.get('text').valid) {
          this.errorMessage = this.errorMessage + "Text "
        }
      }
    )
  }

  validFormMessage() {

  }

  ngOnInit() {
    this.validFormMessage();
  }

  submit(){
    let message:MessageModel = {
      author: {
        name: this.form.value.name,
        color: this.form.value.color
      },
      text: this.form.value.text,
      id: Math.random() + "r",
      timestamp: Math.random()
    };
    // console.dir(message);
    this.form.reset();
    this.messageService.postMessage(message);
  }

}
