import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooComponent } from './foo/foo.component';
import { MessageComponent } from './message/message.component';
import {FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {BarService} from "./bar.service";
import { MessageListComponent } from './message-list/message-list.component';
import {MessagesService} from "./messages-service.service";
import { MessageFormComponent } from './message-form/message-form.component';
import { FormatNumberPipe } from './format-number.pipe';
import { EmojiPipe } from './emoji.pipe';
import {LikesService} from "./likes-service.service";

@NgModule({
  declarations: [
    AppComponent,
    FooComponent,
    MessageComponent,
    MessageListComponent,
    MessageFormComponent,
    FormatNumberPipe,
    EmojiPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [BarService, MessagesService,
    Validators, LikesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
