import { Pipe, PipeTransform } from '@angular/core';
import {reduce} from "rxjs/operators";

@Pipe({
  name: 'emoji'
})
export class EmojiPipe implements PipeTransform {

  replacements = [
    { pattern: /:-?\)/g, emoji: '\uD83D\uDE42' },
    { pattern: /:-?D/g, emoji: '\uD83D\uDE04' },
    { pattern: /;-?\)/g, emoji: '\uD83D\uDE09' },
    { pattern: /XD/g, emoji: '\uD83D\uDE06' },
    { pattern: /:-?\*/g, emoji: '\uD83D\uDE18' },
    { pattern: /\^\^/g, emoji: '\uD83D\uDE0A' },
    { pattern: /:-?P/g, emoji: '\uD83D\uDE1B' },
    { pattern: /;-?P/g, emoji: '\uD83D\uDE1C' },
    { pattern: /:-?\|/g, emoji: '\uD83D\uDE10' },
    { pattern: /8-?\)/g, emoji: '\uD83D\uDE0E' },
    { pattern: /:-?\(/g, emoji: '\uD83D\uDE1E' },
    { pattern: /:-?[oO]/g, emoji: '\uD83D\uDE2E' },
    { pattern: /:'-?\(/g, emoji: '\uD83D\uDE22' },
    { pattern: /></g, emoji: '\uD83D\uDE16' },
    { pattern: /\+1/g, emoji: '\uD83D\uDC4D' },
    { pattern: /-1/g, emoji: '\uD83D\uDC4E' },
    { pattern: /<3/g, emoji: '\uD83D\uDC93' },
  ];

  transform(text: string, args?: any): string{
    if(text === undefined){
      return '';
    }

    let x = this.replacements.reduce((acc, r)=>{
      return acc === null ? '': acc = acc.replace(r.pattern, r.emoji);
    }, text);

    return x;
  }

}
