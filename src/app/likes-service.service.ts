import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {LikeModel} from "./message/LikeModel";
import {WebSocketSubject} from "rxjs/webSocket";
import {MessageModel} from "./message/MessageModel";

@Injectable()
export class LikesService {

  private likesState$ = new BehaviorSubject<LikeModel[]>([]);
  likes$ = this.likesState$.asObservable();

  private socket = new WebSocketSubject<LikeModel>("ws://35.198.179.24/likes/stream");

  constructor() {
    this.socket.subscribe(like => {
      this.likesState$.next([...this.likesState$.value, like]);
    })
  }

  postLike  (like){
    console.log(like);
    this.socket.next({messageId:like});
  }
}
