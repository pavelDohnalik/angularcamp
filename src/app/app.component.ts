import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bootCampDemo Hello';
}

//muze mit dekoratorz pro vstup a vystup (využitý pro poslech udalosti - dekorator @input a @output
