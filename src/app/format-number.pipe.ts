import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatNumber'
})
export class FormatNumberPipe implements PipeTransform {

  transform(value: number, suffix: string = 'k'): string {
    return value > 1000 ? `${Math.round(value/1000)}${suffix}` : value.toString();
  }

}
