import {Component, Input, OnInit} from '@angular/core';
import {BarService} from "../bar.service";

@Component({
  selector: 'app-foo',
  templateUrl: './foo.component.html',
  styleUrls: ['./foo.component.css']
})
export class FooComponent implements OnInit {

  slogan: string = "angular is cool";
  @Input() name: string = "Unkonwn";
  value;
  values = ['a', 'b', 'c'];
  show = false;
  prop = this.bar.prop;


  constructor(private bar: BarService = new BarService()) { }

  ngOnInit() {
    this.value = this.bar.doSome();
  }

}
