import { Injectable } from '@angular/core';
import {randomColor} from "./Utils";
import {MessageModel} from "./message/MessageModel";
import {BehaviorSubject, Observable, of} from "rxjs";
import {WebSocketSubject} from "rxjs/webSocket";





@Injectable()
export class MessagesService {

  now = Date.now();
  private messages: MessageModel[] = [
    {id: '0',
    text: 'text_0',
    timestamp: this.now,
    author: {
      name: 'name_0',
      color: randomColor()
    }},
    {id: '1',
      text: 'text_1',
      timestamp: this.now,
      author: {
        name: 'name_1',
        color: randomColor()
      }},
    {id: '2',
      text: 'text_2',
      timestamp: Date.now(),
      author: {
        name: 'name_2',
        color: randomColor()
      }},
  ];

   // messages$ = of(this.messages);
  messagesState$ = new BehaviorSubject<MessageModel[]>([]);
  messages$ = this.messagesState$.asObservable();
  private socket = new WebSocketSubject<MessageModel>("ws://35.198.179.24/messages/stream");

  constructor() {
    this.socket.subscribe(message => {
      this.messagesState$.next([...this.messagesState$.value, message]);
    });
  }

  postMessage(message: MessageModel){
    this.socket.next(message);
  }





}
