// fix path if necessary
import {Author} from "./Author";

export class MessageModel {
  id?: string;
  timestamp?: number;
  text: string;
  author: Author;
}
