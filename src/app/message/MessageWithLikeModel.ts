import {MessageModel} from "./MessageModel";

export class MessageWithLikeModel extends MessageModel{
  likes: number = 1;
}
