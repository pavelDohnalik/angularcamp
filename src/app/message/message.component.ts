import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Author} from "./Author";
import {MessageModel} from "./MessageModel";
import {MessageWithLikeModel} from "./MessageWithLikeModel";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  @Input() message: MessageWithLikeModel;

  @Output() messageLiked = new EventEmitter();

  constructor() { }

  like() {
    console.dir(this.message);
    this.messageLiked.emit(this.message.id);
  }

  ngOnInit() {
      // const date = new Date();
      // this.message.timestamp = date.getTime();
  }

}
