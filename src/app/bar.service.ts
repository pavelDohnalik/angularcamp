import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BarService {
  public prop = "hello";

  constructor() { }

  doSome(){
    return 3+2;
  }
}
