import {Component, OnDestroy, OnInit} from '@angular/core';
import {MessagesService} from "../messages-service.service";
import {MessageModel} from "../message/MessageModel";
import {Author} from "../message/Author";
import {LikesService} from "../likes-service.service";
import {combineLatest, Observable} from "rxjs";
import {LikeModel} from "../message/LikeModel";
import {MessageWithLikeModel} from "../message/MessageWithLikeModel";
import {map, reduce} from "rxjs/operators";

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit {
  listOfMessages
    = combineLatest(this.likesService.likes$, this.messagesService.messages$).pipe(
      map(([like, mess]) => {
        return mess.map(m => {

          let likes = like.filter(f => f.messageId === m.id).length;
          let messageWithLike: MessageWithLikeModel = {
            id: m.id,
            author: m.author,
            text: m.text,
            timestamp: m.timestamp,
            likes: likes
          };
          return messageWithLike
        });
      })
  );

  listOfmL: MessageModel[] = [];
  newMessage: MessageModel = {
    author:{
      name: '',
      color: "green"
    },
    text: ''
  };

  constructor(private messagesService: MessagesService = new MessagesService()
  ,private likesService: LikesService = new LikesService()) { }

  ngOnInit() {


    //   .subscribe(([like, message])=>{
    //
    //     message.forEach(x=>{
    //       let m: MessageWithLikeModel = {
    //         message: x,
    //         likes: 0
    //       };
    //       like.map(l=> {
    //         if(l.messageId === x.id){
    //             m.likes++;
    //         }
    //       });
    //       this.listOfmL.push(m);
    //     })
    //
    //
    //
    // });

    this.messagesService.messages$.subscribe( data => {
      this.listOfmL = data.sort((a, b) => new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime())
    });
    this.likesService.likes$.subscribe(like=>{
        // console.dir(like);
    })
  }

  sendLike(id) {
    console.log(id);
    this.likesService.postLike(id);
  }

  sendMessage(){
    this.newMessage.author.color="green";
    this.newMessage.id = Math.random()+"s";
    this.messagesService.postMessage(this.newMessage);
    this.newMessage = {
      author:{
        name: '',
        color: "green"
      },
      text: ''
    };
  }

  trackById(index, item){
    return item.id;
  }

}
