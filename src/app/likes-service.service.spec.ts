import { TestBed } from '@angular/core/testing';

import { LikesServiceService } from './likes-service.service';

describe('LikesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LikesServiceService = TestBed.get(LikesServiceService);
    expect(service).toBeTruthy();
  });
});
